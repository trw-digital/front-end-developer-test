# **TRW Front End Developer Test** #

Build a responsive page featuring a booking process.

We’d like you to build a single HTML page that illustrates your thinking and skills around some of the fundamentals of responsive front end development.

The page should feature the following:

A booking form with a 4 stage process

Booking form stage 1 needs to feature:

- Title
- Forename
- Surname
 - Date of birth

Booking form stage 2 needs to feature:

- Date of arrival
- Date of departure

Booking form stage 3 needs to feature:

- Number of guests

Stage 4:

- Review
- Notes
- Submit

The form should feature

- required fields
- Some form of animation (eg. collapsible menu / hide and reveal)

You should output the form content as JSON on submission (this wont need to be submitted to an actual API)

When you have finished, you should zip up your code and email it to karl.loudon@t-rw.com

You have 3 hours to complete the task - enjoy!